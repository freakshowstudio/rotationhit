﻿using UnityEngine;
using System.Collections;

public class RotateCube : MonoBehaviour
{
    [SerializeField] private Vector3 _rotationSpeed = Vector3.one;

    private Transform _transform;

    void Awake()
    {
        _transform = transform;
    }

    void Update()
    {
        // Visualize the faces of the cube
        Debug.DrawRay(
            _transform.position, _transform.right * 2f, Color.red);
        Debug.DrawRay(
            _transform.position, _transform.up * 2f, Color.green);
        Debug.DrawRay(
            _transform.position, _transform.forward * 2f, Color.blue);

        Debug.DrawRay(
            _transform.position, -_transform.right * 2f, Color.magenta);
        Debug.DrawRay(
            _transform.position, -_transform.up * 2f, Color.yellow);
        Debug.DrawRay(
            _transform.position, -_transform.forward * 2f, Color.cyan);


        _transform.Rotate(_rotationSpeed * Time.deltaTime);
    }
}
