﻿using UnityEngine;
using System.Collections;

public class RaycastCube : MonoBehaviour
{
    void Update()
    {
        if (!Input.GetMouseButtonDown(0))
        {
            return;
        }

        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;

        if (!Physics.Raycast(ray, out hit))
        {
            return;
        }

        float dotUp = Vector3.Dot(hit.normal, hit.transform.up);
        float dotRight = Vector3.Dot(hit.normal, hit.transform.right);
        float dotForward = Vector3.Dot(hit.normal, hit.transform.forward);

        bool hitUp = dotUp > 0.99f;
        bool hitDown = dotUp < -0.99f;
        bool hitRight = dotRight > 0.99f;
        bool hitLeft = dotRight < -0.99f;
        bool hitForward = dotForward > 0.99f;
        bool hitBack = dotForward < -0.99f;

        if (hitUp)
        {
            Debug.LogFormat("{0}: Hit Up", Time.realtimeSinceStartup);
        }
        if (hitDown)
        {
            Debug.LogFormat("{0}: Hit Down", Time.realtimeSinceStartup);
        }
        if (hitLeft)
        {
            Debug.LogFormat("{0}: Hit Left", Time.realtimeSinceStartup);
        }
        if (hitRight)
        {
            Debug.LogFormat("{0}: Hit Right", Time.realtimeSinceStartup);
        }
        if (hitForward)
        {
            Debug.LogFormat("{0}: Hit Forward", Time.realtimeSinceStartup);
        }
        if (hitBack)
        {
            Debug.LogFormat("{0}: Hit Back", Time.realtimeSinceStartup);
        }
    }
}
