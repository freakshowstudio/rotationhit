About
=====

A short Unity example showing how you can use a raycast to determine which
side of a cube was clicked.


License
=======

This software is released under the [UNLICENSE](http://unlicense.org/)
license, see the file UNLICENSE for more information.
